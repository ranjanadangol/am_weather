package com.example.amnetworkmanager;

public class WeatherModel {

	String title, desc;
	
	public WeatherModel(String title, String desc){
		super();
		this.title=title;
		this.desc=desc;
	}

	public String getTitle(){
		return title;
	}

	public void setTitle(String t){
		
		this.title=t;
	}
	
	public String getDesc(){
		return desc;
	}
	
	
	public void setDesc(String d){
		
		this.desc=d;
	}
}
