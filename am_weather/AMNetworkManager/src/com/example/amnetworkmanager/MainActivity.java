package com.example.amnetworkmanager;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;


import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	public String TAG = this.getClass().getSimpleName();
    public ProgressDialog pd;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       
        String url = "http://api.openweathermap.org/data/2.5/weather?q=Melbourne,AU";
               
       pd = ProgressDialog.show(this,"Please Wait...","Please Wait...");
        
        final TextView country = (TextView)findViewById(R.id.txtcon);
        final TextView sunrise = (TextView)findViewById(R.id.txtsr);
        final TextView sunset = (TextView)findViewById(R.id.txtss);
        final TextView temp_min = (TextView)findViewById(R.id.txtmin);
        final TextView temp_max = (TextView)findViewById(R.id.txtmax);
        
        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST,url,null,new Response.Listener<JSONObject>() {
            
        	@Override
            public void onResponse(JSONObject response) {
               
                JsonResponse jr=new JsonResponse();
                
                JSONObject val1= jr.sys(response);
                try{
	               	 country.append(""+val1.getString("country"));
	               	 sunrise.append(""+val1.getString("sunrise"));
	               	 sunset.append(""+val1.getString("sunset"));
                }
                catch(JSONException e){
               	 	e.printStackTrace();
                }
                                
                JSONObject val2= jr.main(response);
                try{
	               	 temp_min.append(""+val2.getString("temp_min"));
	               	 temp_max.append(""+val2.getString("temp_max"));
                }
                catch(JSONException e){
               	 	e.printStackTrace();
                }
                
               pd.dismiss();
;            }
        },new Response.ErrorListener() {
        	
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,error.getMessage());
            }
        });
        
     // add the request object to the queue to be executed
		ApplicationController.getInstance().addToRequestQueue(jr);
    }

    	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
