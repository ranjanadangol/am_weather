package com.example.amnetworkmanager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class JsonResponse{
	
	
	public JSONObject coord(JSONObject json){
		
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("coord");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject sys(JSONObject json){
			
			JSONObject value=null;
			try{
				
				value= json.getJSONObject("sys");
						return value;
			}
			catch (JSONException e){
				e.printStackTrace();
			}
			
			return value;		
		}
	
	public JSONObject base(JSONObject json){
		
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("base");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject main(JSONObject json){
		
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("main");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject wind(JSONObject json){
		
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("wind");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject id(JSONObject json){
	
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("id");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject dt(JSONObject json){
	
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("dt");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject clouds(JSONObject json){
	
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("clouds");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject name(JSONObject json){
	
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("name");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject cod(JSONObject json){
	
		JSONObject value=null;
		try{
			
			value= json.getJSONObject("cod");
					return value;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return value;		
	}
	
	public JSONObject Weather(JSONObject json){
		
		JSONArray value;
		try {
			value = json.getJSONArray("weather");
			
			for (int i=0; i<value.length(); i++)
			{
				try{
					
					JSONObject  oneobject=value.getJSONObject(i);
					
					return oneobject;
					
				}
				catch (JSONException e){
					e.printStackTrace();
				}
				
			}
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return null;
	}
    
}