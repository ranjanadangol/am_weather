package com.example.amnetworkmanager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import android.app.Application;
import android.text.TextUtils;


public class ApplicationController extends Application{

	public static final String Tag=null;
	
	/*
	 * Global Request queue for volley
	 * */
	private RequestQueue mrequestqueue;
	
	/*
	 * Singleton instance 
	 * easy access in other places
	 */
	private static ApplicationController sInstance;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		//initialize the singleton
		sInstance=this;
	}
	
	/*
	 * ApplicationController Singleton instance
	 */
	public static synchronized ApplicationController getInstance()
	{
		return sInstance;
	}
	
	/**
     *  The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mrequestqueue == null) {
            mrequestqueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mrequestqueue;
    }
    
    /*
     * Adds the specific request to the global queue 
     * use tag if it is specified
     * else default tag is used 
     */
    
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? Tag : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }
    
    /*
     * Adds specified request to the global queue using the Default Tag
     */
    
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(Tag);

        getRequestQueue().add(req);
    }
   

    /*
     * call all the pending requests by the specified Tag
     * important to specify
     * to cancel pending/ongoing requests 
     */
    public void cancelPendingRequests(Object tag) {
        if (mrequestqueue != null) {
            mrequestqueue.cancelAll(tag);
        }
    }
    
}
