package com.example.amnetworkmanager;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class VolleyAdapter extends BaseAdapter{

	public ArrayList<WeatherModel> wlist;
	public int layout;
	public Context context;

	public VolleyAdapter(Context mcontext, int layout, ArrayList<WeatherModel> arrnews) {
		this.context = mcontext;
		this.wlist = arrnews;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return wlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		if(convertView==null){
			convertView=((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(layout,null);
		}
//		
//		WeatherModel jlist=wlist.get(position);
//		
//		TextView txtTitle=(TextView) convertView.findViewById(R.id.txtTitle);
//		TextView txtDesc=(TextView) convertView.findViewById(R.id.txtDesc);
//		
//		txtTitle.setText(Html.fromHtml(jlist.getTitle()));
//		txtDesc.setText(jlist.getDesc());
		
		return convertView;
	}
	

}